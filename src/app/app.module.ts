import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { PopoverComponent } from './components/home/popover/popover.component';
import { BannerComponent } from './components/home/banner/banner.component';
import { TrialFormComponent } from './components/home/trial-form/trial-form.component';
import { FeaturedComponent } from './components/home/featured/featured.component';
import { SpotlightComponent } from './components/home/spotlight/spotlight.component';
import { ExclusiveComponent } from './components/home/exclusive/exclusive.component';
import { RushBtnComponent } from './components/home/rush-btn/rush-btn.component';
import { Spotlight1Component } from './components/home/spotlight1/spotlight1.component';
import { TilesComponent } from './components/home/tiles/tiles.component';
import { FooterComponent } from './components/home/footer/footer.component';
import { ContactComponent } from './components/home/contact/contact.component';
import { TermsComponent } from './components/home/terms/terms.component';
import { PrivacyComponent } from './components/home/privacy/privacy.component';

import { HomeComponent } from './components/home/home.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CheckoutFormComponent } from './components/checkout/checkout-form/checkout-form.component';
import { ThankyouComponent } from './components/thankyou/thankyou.component';
import { Upsell1Component } from './components/upsell1/upsell1.component';
import { FooterAltComponent } from './components/footer-alt/footer-alt.component';

// Services
import { StatesService } from './services/states.service';
import { AsyncService } from './services/async.service';
import { PageService } from './services/page.service';

// Routes
import { AppRoutes } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    PopoverComponent,
    BannerComponent,
    TrialFormComponent,
    FeaturedComponent,
    SpotlightComponent,
    ExclusiveComponent,
    RushBtnComponent,
    Spotlight1Component,
    TilesComponent,
    FooterComponent,
    ContactComponent,
    TermsComponent,
    PrivacyComponent,
    HomeComponent,
    CheckoutComponent,
    CheckoutFormComponent,
    FooterAltComponent,
    ThankyouComponent,
    Upsell1Component
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutes,
    FormsModule
  ],
  providers: [
    Title,
    StatesService,
    AsyncService,
    PageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
