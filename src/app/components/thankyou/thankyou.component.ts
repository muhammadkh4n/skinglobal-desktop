import { Component, OnInit } from '@angular/core';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {

  date: Date = new Date();
  global: Object = Global;
  currency: any;
  customer: any;

  constructor() { }

  ngOnInit() {
    this.currency = {};
    this.customer = {};
    this.date.setDate(this.date.getDate()+5);
  }

}
