import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Spotlight1Component } from './spotlight1.component';

describe('Spotlight1Component', () => {
  let component: Spotlight1Component;
  let fixture: ComponentFixture<Spotlight1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Spotlight1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Spotlight1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
