import { Component, OnInit } from '@angular/core';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-spotlight1',
  templateUrl: './spotlight1.component.html',
  styleUrls: ['./spotlight1.component.css']
})
export class Spotlight1Component implements OnInit {

  global: Object;

  constructor() { }

  ngOnInit() {
    this.global = Global;
  }

}
