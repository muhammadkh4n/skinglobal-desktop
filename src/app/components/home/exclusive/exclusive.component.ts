import { Component, OnInit } from '@angular/core';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-exclusive',
  templateUrl: './exclusive.component.html',
  styleUrls: ['./exclusive.component.css']
})
export class ExclusiveComponent implements OnInit {

  global: Object;

  constructor() { }

  ngOnInit() {
    this.global = Global;
  }

}
