import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import * as Global from '../../../global_vars/main';
import { PageService } from '../../../services/page.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  global: Object;
  date: Date;

  constructor(private Page: PageService) { }

  ngOnInit() {
    this.global = Global;
    this.date = new Date();
  }

}
