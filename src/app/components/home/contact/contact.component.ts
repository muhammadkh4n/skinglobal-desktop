import { Component, OnInit } from '@angular/core';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  global: Object;

  constructor() { }

  ngOnInit() {
    this.global = Global;
  }

}
