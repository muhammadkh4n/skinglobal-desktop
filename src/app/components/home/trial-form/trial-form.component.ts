import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StatesService } from '../../../services/states.service';
import { AsyncService } from '../../../services/async.service';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-trial-form',
  templateUrl: './trial-form.component.html',
  styleUrls: ['./trial-form.component.css']
})
export class TrialFormComponent implements OnInit {

  statesAbbr: Array<string>;
  states: Object;
  customer: any;

  constructor(private State: StatesService,
              private Async: AsyncService,
              private $router: Router) {}

  ngOnInit() {
    this.State.getStates()
      .subscribe(states => {
        this.states = states;
        this.statesAbbr = Object.keys(this.states);
      });
    let savedForm = JSON.parse(localStorage.getItem('customer'));
    if (savedForm) {
      this.customer = JSON.parse(localStorage.getItem('customer')).body;
    } else {
      this.customer = {};
    }
    this.customer.shipCountry = 'US';
    this.customer.method = 'importLead';
  }

  // Post form data to Async service
  postForm() {
    this.Async.orderTrial(this.customer)
      .subscribe(res => {
        localStorage.setItem('customer', JSON.stringify(res));
        this.$router.navigateByUrl('/checkout');
      });
  }

}
