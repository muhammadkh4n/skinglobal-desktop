import { Component, OnInit } from '@angular/core';

import * as Global from '../../../global_vars/main';

@Component({
  selector: 'app-rush-btn',
  templateUrl: './rush-btn.component.html',
  styleUrls: ['./rush-btn.component.css']
})
export class RushBtnComponent implements OnInit {

  global: Object;

  constructor() { }

  ngOnInit() {
    this.global = Global;
  }

}
