import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RushBtnComponent } from './rush-btn.component';

describe('RushBtnComponent', () => {
  let component: RushBtnComponent;
  let fixture: ComponentFixture<RushBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RushBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RushBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
