import { Component, OnInit } from '@angular/core';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-footer-alt',
  templateUrl: './footer-alt.component.html',
  styleUrls: ['./footer-alt.component.css']
})
export class FooterAltComponent implements OnInit {

  global: Object = Global;
  date: Date = new Date();

  constructor() { }

  ngOnInit() {
  }

}
