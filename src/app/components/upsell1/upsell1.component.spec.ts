import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Upsell1Component } from './upsell1.component';

describe('Upsell1Component', () => {
  let component: Upsell1Component;
  let fixture: ComponentFixture<Upsell1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Upsell1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Upsell1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
