import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';

import * as Global from '../../global_vars/main';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  global: Object;
  randomBetween3and9: number;
  date: Date;
  customer: any;
  discount: any;

  private future: Date;
  private futureString: string;
  private diff: number;
  private $counter: Observable<number>;
  private subscription: Subscription;
  private message: string;

  constructor(
    private route: ActivatedRoute
  ) {
    this.randomBetween3and9 = this.getRandomBetween3and9();
  }

  ngOnInit() {
    this.date = new Date();
    this.global = Global;
    this.date.setDate(this.date.getDate() + 5);
    let savedForm = JSON.parse(localStorage.getItem('customer'));
    if (savedForm) {
      this.customer = JSON.parse(localStorage.getItem('customer')).body;
      this.customer.name = this.customer.firstName + this.customer.lastName;
    } else {
      this.customer = {};
    }
    this.discount = this.route.snapshot.data['discount'];
  }

  ngAfterViewInit() {
    let currDate = new Date();
    this.future = new Date(currDate.setMinutes(currDate.getMinutes() + 5));
    this.$counter = Observable.interval(1000).map((x) => {
      this.diff = Math.floor((this.future.getTime() - new Date().getTime()) / 1000);
      return x;
    });

    this.subscription = this.$counter.subscribe((x) => this.message = this.dhms(this.diff));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  dhms(t) {
    var minutes, seconds;
    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    seconds = t % 60;

    if  (minutes <= 0 && seconds <= 0) {
      this.subscription.unsubscribe();
    }

    return [
      minutes + ':',
      seconds
    ].join('');
  }

  getRandomBetween3and9() {
    return Math.floor(Math.random() * 8) + 3;
  }

}
