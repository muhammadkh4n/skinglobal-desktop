'use strict';

export const name = "Parisian Glow"; //same as corp name minus llc ect.  IE Blossom cream
export const productName = "Parisian Glow Cream";
export const upsell1Name = "Parisian Glow Eye Serum";
export const upsell2Name = "";
export const upsell3Name = "!!UPSELL3NAME";
export const logo = 'logo.png';
export const product1 = 'product1.png';
export const upsell1Btl = 'product2.png';
export const upsell1Ship = 4.97;
export const upsell2Btl = 'product3.png';
export const upsell2Ship = 24.95;
export const upsell3Btl = 'product1.png';
export const upsell3Ship = 4.99;
export const upsell_2_math = upsell2Ship * 2;
export const roundit = upsell_2_math.toFixed(2);
//$orderbottle = '<img src="orderbottle.png">';
//$girl = '<img src="girl.png">';
//$benefitsgraphic = '<img src="benefitsgraphic.png">';
//$header = '<img src="header.png">';
//$button = '<img src="button.png">';
export const price = (0.00).toFixed(2); // Price is for checkout functionality straight sales so it would be 0.00 if its a trial
export const trialPrice = 89.92; // Trialprice is what is in terms
export const checkPrice = 0.00; // Checkout price, 0 for trials duh...just to make math work on step 1
export const shipping = 4.99; //shipping is the same in terms and checkout
export const shipProtection_productPrice = 1.99;
export const totalTrial = 14;
export const minusShipTime = totalTrial - 4;
//$discountprice = "$0.00";
//$shipfee = "$0.00";
export const site = "tryparisianglow.co";
export const phone = "888-436-4265";
export const email = "care@tryparisianglow.com";
export const address = "Corp Address:  ";
export const corpName = "Parisian Glow"; // Full corp name
export const returnaddress = "Fulfillment c/o Parisian Glow<br>PO Box 35482<br>St. Petersburg, FL 33705";
export const version = "v1";