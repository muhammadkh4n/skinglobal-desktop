import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

import * as Global from './global_vars/main';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  date: Date;
  constructor(private titleService: Title) {
    this.setTitle(Global.productName);
    this.date = new Date();
  }

  public setTitle(title: string) {
    this.titleService.setTitle(title);
  }
}
